const express = require("express")
const app = express()
const port = 4001

app.use(express.json())
app.use(express.urlencoded({extended:true}))

app.listen(port, () => console.log(`Server running at port ${port}`));

// GET ROUTE
app.get("/", (req, res) => {
	res.send("Hello, world!");
})

let myArr = []

app.post("/register",(req,res)=>{
	console.log(req.body);
	if(req.body.username !== '' && req.body.password !== '' && req.body.lastName !== '' && req.body.firstName !== '' ){
		myArr.push(req.body);
		res.send(`User ${req.body.firstName} ${req.body.lastName} with username: ${req.body.username} is successfully registered!`);
	} else{
		res.send("Please complete the information required.");
	}
});


app.post("/login",(req,res)=>{
	let message
	for(let i=0;i<myArr.length;i++){

		if(req.body.username == myArr[i].username && req.body.password == myArr[i].password){
			message = `${req.body.username} is now login!`
			break
		}else{
			message = `${req.body.username} is not yet registered`
		}
	}
	res.send(message)
})

