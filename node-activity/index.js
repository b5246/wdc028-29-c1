// #1
fetch('https://jsonplaceholder.typicode.com/posts?id=1',{
	method: 'GET'
})
.then(response=> response.json())
.then(json=>console.log('#1:',json));


// #2
fetch('https://jsonplaceholder.typicode.com/posts/1',{
	method:'PATCH',
	headers:{'Content-Type':'application/json'},
	body:JSON.stringify({
		title:'Updated post',
		body:'Hello World!'
	})
}).then(response=>response.json()).then(json=>console.log('#2',json));







